from django.apps import AppConfig


class SeeResultConfig(AppConfig):
    name = 'see_result'
