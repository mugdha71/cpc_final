import re

from pycparser import c_parser


class CParserObject:
    variables = []
    funcs = []
    parent_node = ''
    modified_code = ''
    file_name = ''

    def __init__(self, f1=None, name="", mod="", func=[], var=[]):
        self.funcs = func
        self.variables = var
        if f1 is not None:
            self.modified_code = self.readFromFile(f1)
            self.file_name = open(f1).name
        else:
            self.modified_code = mod
            self.file_name = name

    def find_fun(self, s, fun):
        regex = r"^([\w\*]+( )*?){2,}\(\)(?!\s*;)|^([\w\*]+( )*?){2,}\(([^!@#$+%^;]+?)\)(?!\s*;)"
        # regex_main = r"^([\w\*]+( )*?){2,}\(\)(?!\s*;)"
        # if fun=='main':
        #     regex=regex_main
        matches = re.finditer(regex, s, re.MULTILINE)
        for matchNum, match in enumerate(matches):
            matchNum = matchNum + 1

            # print("Match {matchNum} was found at {start}-{end}: {match}".format(matchNum=matchNum, start=match.start(),
            #                                                                     end=match.end(), match=match.group()))
            if match.group().__contains__(fun):
                return match.start()
        return -1

    def getSubFunc(self, fun=''):
        s = self.modified_code
        index = self.find_fun(s, fun)
        if index == -1:
            index = self.find_fun(s, fun)
        # index = s.find(fun)
        sub = s[index:]
        chs = list(sub)
        start = 0
        end = 0
        count = 0
        pos = 0
        for c in chs:
            if c == '{':
                if count == 0:
                    start = pos
                count += 1
            elif c == '}':
                count -= 1
                end = pos + 1
            pos += 1
            if count == 0 and start:
                break
        return sub[:end]

    def getValue(self, node):
        self.variables.append(node.name)
        # print('variable: ' + node.name)

    def getFunctionName(self, node):
        if node.name not in self.funcs:
            self.funcs.append(node.name)
        # print('function: ' + node.name)

    def rename_variable(self, src):
        count = 1
        for var in self.variables:
            fnd = r"\b" + var + r"\b"
            rpl = "var_" + str(count)
            count += 1
            src = re.sub(fnd, rpl, src)
        return src

    def rename_func(self, src):
        count = 1
        for fun in self.funcs:
            if fun == 'main':
                continue
            fnd = r"\b" + fun + r"\b"
            rpl = "fun_" + str(count)
            count += 1
            src = re.sub(fnd, rpl, src)
        return src

    def rename_variable_original(self, src):
        count = 1
        for var in self.variables:
            fnd = r"\b" + "var_" + str(count) + r"\b"
            rpl = var
            count += 1
            src = re.sub(fnd, rpl, src)
        return src

    def rename_func_original(self, src):
        count = 1
        for fun in self.funcs:
            if fun == 'main':
                continue
            fnd = r"\b" + "fun_" + str(count) + r"\b"
            rpl = fun
            count += 1
            src = re.sub(fnd, rpl, src)
        return src

    def getAllVariable(self, node):
        for i in node.children():
            if i[1].__class__.__name__ == 'Decl':
                if self.parent_node == "FuncDef":
                    self.getFunctionName(i[1])
                else:
                    self.getValue(i[1])
            self.parent_node = i[1].__class__.__name__
            self.getAllVariable(i[1])

    def readFromFile(self, f1):
        src = ''
        lines_f1 = open(f1).readlines()
        for l in lines_f1:
            # remove include files
            if l.__contains__('#'):
                continue
            # remove empty space
            if l == '\n':
                continue
            # remove comments
            if l.__contains__('//'):
                pos = l.find('//')
                l = l[:pos] + "\n"
            src += l
        # remove multi commentsf
        while src.__contains__("/*"):
            pos = src.find("/*")
            end = src.find("*/")
            delete = src[pos:end + 2]
            src = src.replace(delete, "")
        parser = c_parser.CParser()
        ast = parser.parse(src)
        self.getAllVariable(ast)

        return self.rename_func(self.rename_variable(src))

    def get_all_sub_fun(self):
        subs = []
        counter = 1
        for l in self.funcs:
            if l == 'main':
                subs.append(self.getSubFunc('main'))
            else:
                subs.append(self.getSubFunc('fun_' + str(counter)))
                counter += 1
        # print(self.file_name + str(self.funcs))
        return subs

    def getfunc(self):
        return self.funcs
