import zipfile

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader, RequestContext
from django.core.files.storage import FileSystemStorage

import difflib
import os
from django.template import Template, Context
import datetime
from bs4 import BeautifulSoup
from django.views import View
from tornado import concurrent

from CPC.forms import LoginForms
from .ModifiedLib.Cparser import CParserObject
from .ModifiedLib.comp import BestMatch

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
threshold_value = 0.80

tables = []
matched_per = []
matched_code = []
matched_ref = []
matched_ref_file = []
final_data = []


def match_init(c):
    l = len(c.get_all_sub_fun())
    matched_per.clear()
    matched_code.clear()
    matched_ref.clear()
    matched_ref_file.clear()
    for i in range(l):
        matched_per.append(0.0)
        matched_code.append("")
        matched_ref.append("")
        matched_ref_file.append("")


class Table:
    heading = ''
    table = ''

    def __init__(self, heading, table):
        self.heading = heading
        self.table = table


def comp_2_code(c1, c2):
    if c1.file_name is c2.file_name:
        pass
    all_sub_funs_original = c1.get_all_sub_fun()
    all_sub_funs_ref = c2.get_all_sub_fun()
    bm = BestMatch()
    pos = 0
    for sub in all_sub_funs_original:
        for ref in all_sub_funs_ref:
            if len(sub) > len(ref):
                match = bm.get_best_match(ref, sub, step=5, flex=3)
            else:
                match = bm.get_best_match(sub, ref, step=5, flex=3)
            match_percentage = match[1]
            if match_percentage > threshold_value:
                sm = difflib.SequenceMatcher(None, sub, ref)
                if sm.ratio() > threshold_value:
                    ori = c1.rename_func_original(c1.rename_variable_original(sub))
                    from_copy = c2.rename_func_original(c2.rename_variable_original(ref))
                    if sm.ratio() > match_percentage:
                        match_percentage = sm.ratio()
                    sm = difflib.SequenceMatcher(None, ori, from_copy)
                    if sm.ratio() > match_percentage:
                        match_percentage = sm.ratio()
                    if matched_per[pos] < match_percentage:
                        matched_per[pos] = match_percentage
                        matched_code[pos] = ori
                        matched_ref[pos] = from_copy
                        matched_ref_file[pos] = c2.file_name
                    break
        pos += 1


def comp_code(token):
    global tables
    tables.clear()
    c_files = []
    cpp_files = []
    d = []
    for root, dirs, files in os.walk("upload/all_file/"+token+"/"):
        for file in files:
            if file.endswith('.c'):
                c_files.append(os.path.join(root,file))
            elif file.endswith('.cpp'):
                cpp_files.append(os.path.join(root, file))
            elif file.__contains__('.'):
                d.append(os.path.join(root, file))

    print(d)
    for dd in d:
        os.remove(dd)

    all_files = c_files + cpp_files
    all_cs = []
    for file in all_files:
        try:
            all_cs.append(CParserObject(file))
        except:
            continue

    target_all_cs = all_cs
    for target_c in target_all_cs:
        match_init(target_c)
        for ref_c in all_cs:
            if target_c.file_name is ref_c.file_name:
                continue
            comp_2_code(target_c, ref_c)
        match = []
        for i in range(len(matched_per)):
            if matched_per[i] != 0 and len(matched_code[i]) > 10:
                match.append([matched_per[i],
                              matched_code[i],
                              matched_ref[i],
                              matched_ref_file[i]])
                str_per = "{0:.2f}".format(matched_per[i] * 100)
                heading = "<h3>" + target_c.file_name.replace('upload/all_file/','') + " match from " + matched_ref_file[
                    i].replace('upload/all_file/','') + " (" + str_per + "%)</h3>"
                addTable(matched_code[i], matched_ref[i], heading)
        final_data.append([target_c.file_name, match])


def addTable(sub, ref, heading):
    di = difflib.HtmlDiff().make_file(sub.split('\n'), ref.split('\n'), "Main File", "From copy")
    table = findTable(di)
    global tables
    tables.append(Table(heading, str(table)))


def findTable(html):
    soup = BeautifulSoup(html)
    # print(soup.prettify())
    counter = 0
    table = None
    while table is None:
        id = "difflib_chg_to" + str(counter) + "__top"
        table = soup.find('table', {"id": id})
        counter += 1

    # print(table.prettify())
    return table.prettify()


def getHtml():
    global tables
    boot = """<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>"""
    style = """<style type="text/css">
        table
        { 
            margin-left: auto;
            margin-right: auto;
        }
        table.diff {font-family:Courier; border:medium;}
        .diff_header {background-color:#e0e0e0}
        td.diff_header {text-align:right}
        .diff_next {background-color:#c0c0c0}
        .diff_add {background-color:#aaffaa}
        .diff_chg {background-color:#ffff77}
        .diff_sub {background-color:#ffaaaa}
    </style>"""
    html = """<html><head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title></title>"""
    html += style
    # html += boot
    html += "</head><body>"
    i = 1
    for t in tables:
        html+=exp(i,t.heading,t.table)
        i+=1
        # html += t.heading + '\n'
        # html += t.table + '\n'
    html += '</body></html>'
    return html


def exp(id ,head,table):
    res = """
<div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#"""+str(id)+"""">
          """+head+"""</a>
        </h4>
      </div>
      <div id=\""""+str(id)+"""" class="panel-collapse collapse">
"""
    res += table
    res += """</div>
    </div>
</div>"""
    return res


def run_code(token):
    start = datetime.datetime.now()
    comp_code(token)
    # f = open("upload/result.html", "w+")
    # f.write("Hello")
    # f.close()
    end = datetime.datetime.now()
    diff = end-start
    print(diff)


def show(request):
    if not request.user.is_authenticated:
        return redirect('../login')

    if request.method == 'POST':
        token = request.POST.get("token")
        return index(request,token)
    return render(request, 'upload/input_for_show_code.html')

def index(request,token):

    print('====Start====')
    run_code(token)
    print('====End====')
    start = """{% extends "upload/test.html" %}
{% block body %}
"""
    end = """
{% endblock body %}
    """
    html_data=getHtml()
    template = Template(start+html_data+end)
    import time
    timestr = time.strftime("%Y%m%d_%H%M%S")
    save_file_path = "upload/saved_results/"+str(request.user)+"/"
    if not os.path.exists(save_file_path):
        os.mkdir(save_file_path)
    f = open(save_file_path+token+"_"+timestr+".html", "w+")
    f.write(html_data)
    f.close()
    return HttpResponse(template.render(RequestContext(request)))


def unzip_member_f3(zip_filepath, filename, dest):
    try:
        with open(zip_filepath, 'rb') as f:
            zf = zipfile.ZipFile(f)
            zf.extract(filename, dest)
    except:
        pass
    fn = os.path.join(dest, filename)
    return _count_file(fn)
def _count_file(fn):
    try:
        return len(os.listdir(fn))
    except:
        return -1
    # with open(fn, 'rb') as f:
    #     return _count_file_object(f)

def _count_file_object(f):
    # Note that this iterates on 'f'.
    # You *could* do 'return len(f.read())'
    # which would be faster but potentially memory
    # inefficient and unrealistic in terms of this
    # benchmark experiment.
    total = 0
    for line in f:
        total += len(line)
    return total

def f3(fn, dest):
    folder_name= fn.split('/')[-1].split('.')[0]
    dest+='/'+folder_name
    with open(fn, 'rb') as f:
        zf = zipfile.ZipFile(f)
        futures = []
        with concurrent.futures.ProcessPoolExecutor() as executor:
            for member in zf.infolist():
                futures.append(
                    executor.submit(
                        unzip_member_f3,
                        fn,
                        member.filename,
                        dest,
                    )
                )
            total = 0
            for future in concurrent.futures.as_completed(futures):
                total += future.result()
    return total
def unzip(req):
    f3('upload/all_file/testcele.zip','upload/all_file/unzip/')
    return HttpResponse("<h1>Home "+"Done"+"</h1>")
def simple_upload(request):
    if not request.user.is_authenticated:
        return redirect('../login')
    if request.method == 'POST' and request.FILES['myfile']:

        myfiles = request.FILES.getlist('myfile')
        reg = request.POST.get("reg")
        # name = request.POST.get("name")
        # dept = request.POST.get("dept")
        token = request.POST.get("token")

        fs = FileSystemStorage()
        uploaded_file_url = []
        for myfile in myfiles:
            # print(myfile)
            if myfile.name.endswith('.zip'):
                filename = fs.save('upload/all_file/'+token+'/zipped_files/' + myfile.name, myfile)
                uploaded_file_url.append(fs.url(filename))
            if myfile.name.endswith('.c') or myfile.name.endswith('.cpp'):
                filename = fs.save('upload/all_file/'+token+'/' + reg + "/" + myfile.name, myfile)
                uploaded_file_url.append(fs.url(filename))
        dir_path = 'upload/all_file/'+token+'/zipped_files/'
        if os.path.exists(dir_path):
            for f in os.listdir(dir_path):
                if f.endswith('.zip'):
                    f3(dir_path+f,'upload/all_file/'+token+'/')

                    os.remove(dir_path+f)
            os.rmdir(dir_path)
        return render(request, 'upload/thankyou.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'upload/upload.html')


def about(request):
    return render(request, 'upload/about.html')


def faq(request):
    return render(request, 'upload/faq.html')


class LoginView(View):
    form_class = LoginForms
    template = 'upload/login.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid() or True:
            print('Valid form')
            # user = form.save(commit=False)

            username = form.data['username']
            password = form.cleaned_data['password']
            print(username)
            # user.set_password(password)
            # user.save()

            user = authenticate(username=username, password=password)
            if user is not None:

                print('Valid user')
                if user.is_active:
                    login(request, user)
                    print(request.user.is_authenticated)
                    return redirect('../')
        else:
            print(form.errors)
        return render(request, self.template, {'form': form})


def home(request):
    user = request.user.is_authenticated
    logout(request)
    return HttpResponse("<h1>Home "+str(user)+"</h1>")

def logout_fun(request):
    logout(request)
    return redirect('../')

def create_saved_list(list=None):
    start = """{% extends "upload/test.html" %}
    {% block body %}
    """
    end = """
    {% endblock body %}
        """
    result = "<h1>Your saved results</h1>"
    if list is None:
        note = "<h4>Your don't have any saved file</h4>"
        return start+result+note+end
    for index,i in zip(range(len(list)),list):
        result+='<ul><a href="'+str(index)+'">'+i+'</a></ul>'
    return start+result+end

def saved_result(request):
    if not request.user.is_authenticated:
        return redirect('../login')
    save_file_path = "upload/saved_results/" + str(request.user) + "/"
    if os.path.exists(save_file_path):
        all_file = os.listdir(save_file_path)
        if len(all_file)>0:
            return HttpResponse(Template(create_saved_list(all_file)).render(RequestContext(request)))
    return HttpResponse(Template(create_saved_list()).render(RequestContext(request)))


def show_saved_result(req,pos):
    save_file_path = "upload/saved_results/" + str(req.user) + "/"
    if os.path.exists(save_file_path):
        all_file = os.listdir(save_file_path)
        if len(all_file)>pos:
            start = """{% extends "upload/test.html" %}
            {% block body %}
            """
            end = """
            {% endblock body %}
                """
            f = open(save_file_path+all_file[pos], "r")
            html_data = f.read()
            # html_data = "<h1>Hello</h1>"
            template = Template(start + html_data + end)
            # return render(req, 'upload/test.html', {
            #     'data': html_data
            # })
            c = RequestContext(req)
            return HttpResponse(template.render(c))

    return HttpResponse(Template(create_saved_list()).render(RequestContext(req)))