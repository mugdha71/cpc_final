import difflib
import glob
import os
from django.template import Template, Context
import datetime
from bs4 import BeautifulSoup
from .Cparser import CParserObject
from .comp import BestMatch

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
threshold_value = 0.80

tables = []
matched_per = []
matched_code = []
matched_ref = []
matched_ref_file = []
final_data = []


def match_init(c):
    l = len(c.get_all_sub_fun())
    matched_per.clear()
    matched_code.clear()
    matched_ref.clear()
    matched_ref_file.clear()
    for i in range(l):
        matched_per.append(0.0)
        matched_code.append("")
        matched_ref.append("")
        matched_ref_file.append("")


class Table:
    heading = ''
    table = ''

    def __init__(self, heading, table):
        self.heading = heading
        self.table = table


def comp_2_code(c1, c2):
    if c1.file_name is c2.file_name:
        pass
    all_sub_funs_original = c1.get_all_sub_fun()
    all_sub_funs_ref = c2.get_all_sub_fun()
    bm = BestMatch()
    pos = 0
    for sub in all_sub_funs_original:
        for ref in all_sub_funs_ref:
            if len(sub) > len(ref):
                match = bm.get_best_match(ref, sub, step=5, flex=3)
            else:
                match = bm.get_best_match(sub, ref, step=5, flex=3)
            match_percentage = match[1]
            if match_percentage > threshold_value:
                sm = difflib.SequenceMatcher(None, sub, ref)
                if sm.ratio() > threshold_value:
                    ori = c1.rename_func_original(c1.rename_variable_original(sub))
                    from_copy = c2.rename_func_original(c2.rename_variable_original(ref))
                    if sm.ratio() > match_percentage:
                        match_percentage = sm.ratio()
                    sm = difflib.SequenceMatcher(None, ori, from_copy)
                    if sm.ratio() > match_percentage:
                        match_percentage = sm.ratio()
                    if matched_per[pos] < match_percentage:
                        matched_per[pos] = match_percentage
                        matched_code[pos] = ori
                        matched_ref[pos] = from_copy
                        matched_ref_file[pos] = c2.file_name
                    break
        pos += 1


def comp_code():
    c_files = glob.glob("upload/all_file/*/*.c")
    cpp_files = glob.glob("upload/all_file/*/*.cpp")
    all_files = c_files + cpp_files
    all_cs = []
    for file in all_files:
        try:
            all_cs.append(CParserObject(file))
        except:
            continue

    target_all_cs = all_cs
    for target_c in target_all_cs:
        match_init(target_c)
        for ref_c in all_cs:
            if target_c.file_name is ref_c.file_name:
                continue
            comp_2_code(target_c, ref_c)
        match = []
        for i in range(len(matched_per)):
            if matched_per[i] != 0 and len(matched_code[i]) > 10:
                match.append([matched_per[i],
                              matched_code[i],
                              matched_ref[i],
                              matched_ref_file[i]])
                str_per = "{0:.2f}".format(matched_per[i] * 100)
                heading = "<h3>" + target_c.file_name.replace('upload/all_file/','') + " match from " + matched_ref_file[
                    i].replace('upload/all_file/','') + " (" + str_per + "%)</h3>"
                addTable(matched_code[i], matched_ref[i], heading)
        final_data.append([target_c.file_name, match])


def addTable(sub, ref, heading):
    di = difflib.HtmlDiff().make_file(sub.split('\n'), ref.split('\n'), "Main File", "From copy")
    table = findTable(di)
    global tables
    tables.append(Table(heading, str(table)))


def findTable(html):
    soup = BeautifulSoup(html)
    # print(soup.prettify())
    counter = 0
    table = None
    while table is None:
        id = "difflib_chg_to" + str(counter) + "__top"
        table = soup.find('table', {"id": id})
        counter += 1

    # print(table.prettify())
    return table.prettify()


def getHtml():
    global tables
    boot = """<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>"""
    style = """<style type="text/css">
        table
        { 
            margin-left: auto;
            margin-right: auto;
        }
        table.diff {font-family:Courier; border:medium;}
        .diff_header {background-color:#e0e0e0}
        td.diff_header {text-align:right}
        .diff_next {background-color:#c0c0c0}
        .diff_add {background-color:#aaffaa}
        .diff_chg {background-color:#ffff77}
        .diff_sub {background-color:#ffaaaa}
    </style>"""
    html = """<html><head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title></title>"""
    html += style
    html += boot
    html += "</head><body>"
    i = 1
    for t in tables:
        html+=exp(i,t.heading,t.table)
        i+=1
        # html += t.heading + '\n'
        # html += t.table + '\n'
    html += '</body></html>'
    return html


def exp(id ,head,table):
    res = """
<div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" href="#"""+str(id)+"""">
          """+head+"""</a>
        </h4>
      </div>
      <div id=\""""+str(id)+"""" class="panel-collapse collapse">
"""
    res += table
    res += """</div>
    </div>
</div>"""
    return res


def run_code():
    start = datetime.datetime.now()
    comp_code()
    # f = open("upload/result.html", "w+")
    # f.write("Hello")
    # f.close()
    end = datetime.datetime.now()
    diff = end-start
    print(diff)

