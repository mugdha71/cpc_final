# How to use: 
## Admin: 
##### 1.First go to project directory and open terminal or command prompt. 

##### 2.Create super user by this command
```bash
python manage.py createsuperuser 
``` 
It will ask username and password. 

##### 3.Now runnserver by this command 
```bash
python manage.py runserver 192.168.0.1:8000
#(change your IP address)
```

##### 4.Then go to /CPC/CPC/settings.py 

##### 5.Add your IP address to [ALLOWED_HOSTS]() list 

##### 6.Now go to [http://192.168.0.1:8000/admin](http://192.168.0.1:8000/admin) to add new user or modify user. 

## User: 
##### 1.Get username and password from admin. 
##### 2.Take files (*.c , *.cpp) from students in a zip file. Recommended: name of the zip file as unique.
##### 3.Store all zip file in a new folder. 
##### 4.Now upload that folder. It will upload all file (*.zip, *.c, *.cpp) from that folder. 
##### 5.After upload complete click [See All Results]()
###### Web Engineering Project [SUST](https://www.sust.edu/) 